<?php

namespace App\Helpers;

use App\User;
use Symfony\Component\HttpFoundation\ParameterBag;

class LoginHelper
{
    public static function checkUser($credentials)
    {
        /** @var \App\User $user */
        $user = User::where('email', $credentials['email'])->first();

        if ($user instanceof User)
        {
            return password_verify($credentials['password'], $user->password) ? $user : null;
        }

        return null;
    }
}