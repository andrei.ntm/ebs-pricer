<?php

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

if (! function_exists('abort_api')) {
    function abort_api($code, $message = '')
    {
        throw new HttpResponseException(response(compact('message'), $code));
    }
}