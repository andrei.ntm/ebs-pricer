<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use Symfony\Component\HttpFoundation\ParameterBag;

class GroupDatesHelper
{
    /**
     * @param $prices
     * @return array
     */
    public static function byWeek($prices)
    {
        $data = [];

        foreach ($prices as $price)
        {
            $date = Date::createFromFormat('Y-m-d', $price['date']);


            $weekIndex = self::multi_array_search_key($data, $date);

            if (is_numeric($weekIndex)) {
                $data[$weekIndex]['amount'][] = $price['amount'];

                continue;
            }

            $data[] = [
                'from' => $date->startOfWeek()->format('Y-m-d'),
                'to' => $date->endOfWeek()->format('Y-m-d'),
                'amount' => [$price['amount']]
            ];
        }

        return $data;
    }

    /**
     * @param $prices
     * @return array
     */
    public static function byMonth($prices)
    {
        $data = [];

        foreach ($prices as $price)
        {
            $date = Date::createFromFormat('Y-m-d', $price['date']);


            $monthIndex = self::multi_array_search_key($data, $date);

            if (is_numeric($monthIndex)) {
                $data[$monthIndex]['amount'][] = $price['amount'];

                continue;
            }

            $data[] = [
                'from' => $date->startOfMonth()->format('Y-m-d'),
                'to' => $date->endOfMonth()->format('Y-m-d'),
                'amount' => [$price['amount']]
            ];
        }

        return $data;
    }

    /**
     * @param $array
     * @param \Illuminate\Support\Carbon $date
     * @return bool|int|string|null
     */
    private static function multi_array_search_key($array, Carbon $date)
    {
        $searchArray = array_filter($array, function ($subarray, $key) use ($date) {
           return $date->format('Y-m-d') >= $subarray['from'] && $date->format('Y-m-d') <= $subarray['to'];
        }, ARRAY_FILTER_USE_BOTH);

        if (!empty($searchArray)) {
            return key($searchArray);
        }

        return false;
    }
}