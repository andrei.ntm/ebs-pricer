<?php

namespace App\Http\Requests\ProductPrice;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guard('api')->check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|numeric|min:0',
            'from' => 'required_with:to|date|before_or_equal:to',
            'to' => 'required_with:from|date|after_or_equal:from|before:today'
        ];
    }
}