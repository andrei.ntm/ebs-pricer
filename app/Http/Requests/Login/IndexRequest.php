<?php

namespace App\Http\Requests\Login;

use App\Http\Requests\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email|exists:users",
            "password" => "required"
        ];
    }
}