<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guard('api')->check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|numeric|min:0'
        ];
    }
}