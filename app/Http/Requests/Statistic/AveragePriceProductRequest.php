<?php

namespace App\Http\Requests\Statistic;

use App\Http\Requests\FormRequest;
use App\Utils\StatisticType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class AveragePriceProductRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guard('api')->check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in([StatisticType::WEEK, StatisticType::MONTH, StatisticType::CUSTOM])
            ],
            'from' => [
                Rule::requiredIf(function () {
                    return $this->has('type') && in_array($this->get('type'), [StatisticType::CUSTOM]);
                }),
                'date',
                'before_or_equal:to'
            ],
            'to' => [
                Rule::requiredIf(function () {
                    return $this->has('type') && in_array($this->get('type'), [StatisticType::CUSTOM]);
                }),
                'date',
                'after_or_equal:from',
                'before_or_equal:today'
            ]
        ];
    }
}