<?php

namespace App\Http\Requests\Statistic;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Auth;

class PriceProductRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guard('api')->check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'from' => 'required|date|before_or_equal:to',
            'to' => 'required|date|after_or_equal:from|before_or_equal:today'
        ];
    }
}