<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryPrice\UpdateRequest;
use App\Product;
use App\Services\PriceService;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;

class CategoryPriceController extends Controller
{
    /**
     * @var \App\Services\PriceService
     */
    protected $priceService;

    /**
     * CategoryPriceController constructor.
     * @param \App\Services\PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * @param $category
     * @param \App\Http\Requests\CategoryPrice\UpdateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($category, UpdateRequest $request): Response
    {
        /** @var Category $category */
        $category = Category::find($category) ?? abort_api(Response::HTTP_NOT_FOUND, 'Category not found');

        $this->priceService->updateByCategory($category, new ParameterBag($request->only('from', 'to', 'amount')));

        return response($category, Response::HTTP_OK);
    }
}