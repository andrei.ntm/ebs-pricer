<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateRequest;
use App\Product;
use App\Services\PriceService;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ProductController
{
    /**
     * @var \App\Services\PriceService
     */
    protected $priceService;

    /**
     * @var \App\User
     */
    protected $user;

    /**
     * ProductController constructor.
     * @param \App\Services\PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
        $this->user = Auth::guard('api')->user();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(): Response
    {
        return response(Product::all(), Response::HTTP_OK);
    }

    /**
     * @param \App\Http\Requests\Product\CreateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CreateRequest $request): Response
    {
        $product = Product::create($request->only('title', 'category_id'));

        $this->priceService->createByProduct($product, $request->get('price'));

        return response($product, Response::HTTP_CREATED);
    }
}