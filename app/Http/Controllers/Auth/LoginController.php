<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\LoginHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Login\IndexRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @param \App\Http\Requests\Login\IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        /** @var User $user */
        $user = LoginHelper::checkUser($request->only('email', 'password'));

        if ($user instanceof User) {
            $token = Auth::guard('api')->setUser($user)->user()->createToken('EBS-PRICER')->accessToken;

            return response()->json(['token' => $token], Response::HTTP_OK);
        }

        return response()->json(['error' => 'Unauthorised'], Response::HTTP_UNAUTHORIZED);
    }
}