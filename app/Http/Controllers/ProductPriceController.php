<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductPrice\UpdateRequest;
use App\Product;
use App\Services\PriceService;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;

class ProductPriceController extends Controller
{
    /**
     * @var \App\Services\PriceService
     */
    protected $priceService;

    /**
     * ProductPriceController constructor.
     * @param \App\Services\PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * @param $product
     * @param \App\Http\Requests\ProductPrice\UpdateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($product, UpdateRequest $request): Response
    {
        /** @var Product $product */
        $product = Product::find($product) ?? abort_api(Response::HTTP_NOT_FOUND, 'Product not found');

        $this->priceService->updateByProduct($product, new ParameterBag($request->only('from', 'to', 'amount')));

        return response($product->load('prices'), Response::HTTP_OK);
    }
}