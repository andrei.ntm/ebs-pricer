<?php

namespace App\Http\Controllers;

use App\Http\Requests\Statistic\AveragePriceProductRequest;
use App\Http\Requests\Statistic\PriceProductRequest;
use App\Product;
use App\Services\StatisticService;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;

class StatisticController extends Controller
{
    /**
     * @var \App\Services\StatisticService
     */
    protected $statisticService;

    /**
     * StatisticController constructor.
     * @param \App\Services\StatisticService $statisticService
     */
    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    /**
     * @param $product
     * @param \App\Http\Requests\Statistic\PriceProductRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function priceProduct($product, PriceProductRequest $request): Response
    {
        /** @var Product $product */
        $product = Product::find($product) ?? abort_api(Response::HTTP_NOT_FOUND, 'Product not found');

        $result = $this->statisticService->pricePerDay($product, new ParameterBag($request->only('from', 'to')));

        return response($result, Response::HTTP_OK);
    }

    /**
     * @param $product
     * @param \App\Http\Requests\Statistic\AveragePriceProductRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function averagePriceProduct($product, AveragePriceProductRequest $request): Response
    {
        /** @var Product $product */
        $product = Product::find($product) ?? abort_api(Response::HTTP_NOT_FOUND, 'Product not found');

        $result = $this->statisticService->averagePrice($product, $request);

        return response($result, Response::HTTP_OK);
    }
}