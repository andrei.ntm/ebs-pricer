<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Category\CreateRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @var \App\User
     */
    protected $user;

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard('api')->user();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(): Response
    {
        return response(Category::all(), Response::HTTP_OK);
    }

    /**
     * @param \App\Http\Requests\Category\CreateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CreateRequest $request): Response
    {
        /** @var Category $category */
        $category = Category::create([
            'title' => $request->get('title'),
            'user_id' => $this->user->id,
        ]);

        return response($category, Response::HTTP_CREATED);
    }
}