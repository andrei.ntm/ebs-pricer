<?php

namespace App\Utils;

class StatisticType
{
    const WEEK = 'week';
    const MONTH = 'month';
    const CUSTOM = 'custom';
}