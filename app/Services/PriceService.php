<?php

namespace App\Services;

use App\Category;
use App\Price;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Symfony\Component\HttpFoundation\ParameterBag;

class PriceService
{
    /**
     * @param \App\Product $product
     * @param $priceAmount
     * @return \App\Price
     */
    public function createByProduct(Product $product, $priceAmount): Price
    {
        $price = Price::create([
            'amount' => $priceAmount,
            'product_id' => $product->id,
            'from' => Date::now()
        ]);

        return $price;
    }

    /**
     * @param \App\Product $product
     * @param \Symfony\Component\HttpFoundation\ParameterBag $parameterBag
     * @return \App\Product
     */
    public function updateByProduct(Product $product, ParameterBag $parameterBag)
    {
        if ($parameterBag->has('from') && $parameterBag->has('to')) {
            $price = Price::create([
                'amount' => $parameterBag->get('amount'),
                'from' => $parameterBag->get('from'),
                'to' => $parameterBag->get('to'),
                'product_id' => $product->id
            ]);

            return $product;
        }

        /** @var Price $lastPrice */
        $lastPrice = $product->prices()->whereNull('to')->first();
        $lastPrice->to = Date::now();
        $lastPrice->save();

        $price = Price::create([
            'amount' => $parameterBag->get('amount'),
            'from' => Date::now(),
            'product_id' => $product->id
        ]);

        return $product;
    }

    /**
     * @param \App\Category $category
     * @param \Symfony\Component\HttpFoundation\ParameterBag $parameterBag
     * @return \App\Category
     */
    public function updateByCategory(Category $category, ParameterBag $parameterBag)
    {
        /** @var Product $product */
        foreach ($category->products as $product) {
            $this->updateByProduct($product, $parameterBag);
        }

        return $category;
    }
}