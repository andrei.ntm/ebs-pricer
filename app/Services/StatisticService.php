<?php

namespace App\Services;

use App\Helpers\GroupDatesHelper;
use App\Product;
use App\Utils\StatisticType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Symfony\Component\HttpFoundation\ParameterBag;

class StatisticService
{
    /**
     * @param \App\Product $product
     * @param \Symfony\Component\HttpFoundation\ParameterBag $parameterBag
     * @return array
     */
    public function pricePerDay(Product $product, ParameterBag $parameterBag = null)
    {
        $to = $parameterBag instanceof ParameterBag && $parameterBag->has('to') ? $parameterBag->get('to') : Date::now();

        $prices = $this->getPrices($product, $parameterBag);
        $data = [];

        /** @var \App\Price $price */
        foreach ($prices as $price) {$fromDate = Date::createFromFormat('Y-m-d', $price->from);
            $toDate = is_null($price->to) ? $to : Date::createFromFormat('Y-m-d', $price->to);

            for ($i = $fromDate; $i <= $toDate; $i = $fromDate->addDay()) {
                if (is_numeric(array_search($i->format('Y-m-d'), array_column($data, 'date')))) {
                    continue;
                }

                $data[] = [
                    'date' => $i->format('Y-m-d'),
                    'amount' => $price->amount,
                ];
            }
        }

        return $data;
    }

    /**
     * @param \App\Product $product
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function averagePrice(Product $product, Request $request)
    {
        switch ($request->get('type'))
        {
            case StatisticType::WEEK:
                $data = $this->averagePriceWeek($product);
                break;
            case StatisticType::MONTH:
                $data = $this->averagePriceMonth($product);
                break;
            case StatisticType::CUSTOM:
                $data = $this->averagePriceCustom($product, new ParameterBag($request->only('from', 'to')));
                break;
            default:
                $data = [];
        }

        return $data;
    }

    /**
     * @param \App\Product $product
     * @return array
     */
    private function averagePriceMonth(Product $product)
    {
        $prices = $this->pricePerDay($product);

        $data = GroupDatesHelper::byMonth($prices);

        return array_map(function ($entry) {
            return [
                'week' => sprintf('%s - %s', $entry['from'], $entry['to']),
                'average_price' => money_format('%.2n', array_sum($entry['amount']) / count($entry['amount']))
            ];
        }, $data);
    }

    /**
     * @param \App\Product $product
     * @return array
     */
    private function averagePriceWeek(Product $product)
    {
        $prices = $this->pricePerDay($product);

        $data = GroupDatesHelper::byWeek($prices);

        return array_map(function ($entry) {
            return [
                'week' => sprintf('%s - %s', $entry['from'], $entry['to']),
                'average_price' => money_format('%.2n', array_sum($entry['amount']) / count($entry['amount']))
            ];
        }, $data);
    }

    /**
     * @param \App\Product $product
     * @param \Symfony\Component\HttpFoundation\ParameterBag $parameterBag
     * @return array
     */
    private function averagePriceCustom(Product $product, ParameterBag $parameterBag)
    {
        $pricesPerDay = $this->pricePerDay($product, $parameterBag);

        return [
            'from' => $parameterBag->get('from'),
            'to' =>$parameterBag->get('to'),
            'average' => money_format('%.2n', array_sum(array_column($pricesPerDay, 'amount')) / count($pricesPerDay)),
        ];
    }

    /**
     * @param \App\Product $product
     * @param \Symfony\Component\HttpFoundation\ParameterBag|null $parameterBag
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getPrices(Product $product, ParameterBag $parameterBag = null)
    {
        //Can be moved to a Repository.
        $prices = $product->prices();

        if ($parameterBag instanceof ParameterBag && $parameterBag->has('from') && $parameterBag->has('to')) {
            $prices
                ->whereBetween('from', [$parameterBag->get('from'), $parameterBag->get('to')])
                ->where(function (Builder $q) use ($parameterBag) {
                    $q
                        ->whereBetween('to', [$parameterBag->get('from'), $parameterBag->get('to')])
                        ->orWhere(function (Builder $q) use ($parameterBag) {
                            $q->whereNull('to')->where('from', '<=', $parameterBag->get('to'));
                        });
                });
        }

        return $prices->orderByDesc('from')
            ->orderByDesc('created_at')
            ->get();
    }
}