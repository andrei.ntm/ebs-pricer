DOCKER_COMPOSE = docker-compose -p photo
TOOLS = $(DOCKER_COMPOSE) run --rm app
CONSOLE = $(TOOLS) php artisan

up:
	@$(DOCKER_COMPOSE) up -d --no-recreate
down:
	@$(DOCKER_COMPOSE) down --remove-orphans
build:
	@$(DOCKER_COMPOSE) build --force-rm
sh:
	@$(DOCKER_COMPOSE) exec app bash
logs:
	@$(DOCKER_COMPOSE) logs -f
migrate:
	@$(CONSOLE) php artisan migrate