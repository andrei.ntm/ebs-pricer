# EBS pricer API 

## Development environment

### System requirements
<ul>
    <li>Linux (Ubuntu 18.04LTS), Windows 10 or MacOS</li>
    <li><a href="https://docs.docker.com/install/linux/docker-ce/ubuntu/ target="_blank">Docker</a> version 18 or newer </li>
    <li><a href="https://docs.docker.com/compose/install/#install-compose" target="_blank">Docker compose</a> version 1.23 or newer</li>
    <li><code>make</code> tool (For Ubuntu run: <code>$sudo apt-get install make</code>, for windows 10 download : <a href="http://gnuwin32.sourceforge.net/packages/make.htm" target="_blank">Make for Windows</a> )</li>
</ul>

### Build
Run `make build` or `docker-compose build`

### Run application

Run the backend API:

```
make up
# After the first run also run this command to create the intial data
```

Navigate to `http://localhost:8091`

### Build application
Scope of build is to create fresh configuration of entire docker environment

Run `make build`

### Run application
Run `make up`

### Stop application
Run `make down`

### Go inside docker container of the application
Run `make sh`

### Run migrations
Run `make migrate`

### Show logs of all containers
Run `make logs`

### Instruction to run project
Run `make build up` in terminal, import postman collection and environment from project folder, go to Auth->Register User in postman.
Register a user and save the token value in postman environment.