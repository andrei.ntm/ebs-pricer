#!/bin/sh
set -e

composer install --prefer-dist --no-progress --no-suggest --no-interaction
chmod 777 -R storage

service cron start

php artisan migrate
php artisan passport:client --personal --name=EBS-PRICER

exec docker-php-entrypoint "$@"