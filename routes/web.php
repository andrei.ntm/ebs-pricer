<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */
$router->group(['prefix' => 'v1'], function () use ($router) {
    $router->post('register', 'Auth\RegisterController@index');
    $router->post('login', 'Auth\LoginController@index');

    $router->group(['prefix' => 'categories'], function () use ($router) {
        $router->get('/', 'CategoryController@index');
        $router->post('/', 'CategoryController@create');
    });

    $router->group(['prefix' => 'products'], function () use ($router) {
        $router->get('/', 'ProductController@index');
        $router->post('/', 'ProductController@create');
    });

    $router->group(['prefix' => 'product-prices'], function () use ($router) {
        $router->patch('/{product}', 'ProductPriceController@update');
    });

    $router->group(['prefix' => 'category-prices'], function () use ($router) {
        $router->patch('/{category}', 'CategoryPriceController@update');
    });

    $router->group(['prefix' => 'statistics'], function () use ($router) {
       $router->post('/price-product/{product}', 'StatisticController@priceProduct');
       $router->post('/average-price-product/{product}', 'StatisticController@averagePriceProduct');
    });
});

$router->get('/', function () {
   return 'Have fun ;)';
});